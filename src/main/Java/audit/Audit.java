package audit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "audit", schema = "project")
public class Audit implements Serializable {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "COMMITER")
    private String commiter;

    @Column(name = "JSONUSER")
    private String JSONUSER;

    @Column(name = "TIMEOFACTION")
    private String timeOfAction;

    public Audit(String action, String commiter, String JSONUSER, String timeOfAction) {
        this.action = action;
        this.commiter = commiter;
        this.JSONUSER = JSONUSER;
        this.timeOfAction = timeOfAction;
    }

    public Audit() {
    }

    public Audit(int id, String action, String commiter, String JSONUSER, String timeOfAction) {
        this.id = id;
        this.action = action;
        this.commiter = commiter;
        this.JSONUSER = JSONUSER;
        this.timeOfAction = timeOfAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCommiter() {
        return commiter;
    }

    public void setCommiter(String commiter) {
        this.commiter = commiter;
    }

    public String getJSONUSER() {
        return JSONUSER;
    }

    public void setJSONUSER(String JSONUSER) {
        this.JSONUSER = JSONUSER;
    }

    public String getTimeOfAction() {
        return timeOfAction;
    }

    public void setTimeOfAction(String timeOfAction) {
        this.timeOfAction = timeOfAction;
    }
}
