package users;

import groups.Group;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

public class UserView {

    private int usrId;

    private int age;

    private String email;

    private String phone;

    private String role;

    private String userName;

    private List<Group> groups;


    public UserView(int usrId, int age, String email, String phone, String role, String userName, List<Group> groups) {
        this.usrId = usrId;
        this.age = age;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.userName = userName;
        this.groups = groups;
    }


    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
