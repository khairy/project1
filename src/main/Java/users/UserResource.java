package users;

import Exceptions.OldPasswordsDoesNotMatch;
import Exceptions.UnAuthorized;
import Exceptions.UserNameAlreadyExists;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class UserResource implements UserInterface {

    private static final Logger LOGGER = Logger.getLogger(UserRepository.class.getName());

    @EJB
    private UserRepository repo;

    @Context
    HttpServletRequest request;

    @Context
    SecurityContext securityContext;

    @Override
    public Response getAllUsers() {
        try {
            System.out.println("Getting all Users");
            List<User> users = repo.getAllUsers();
            List<UserView> usersView = new ArrayList<>();
            for (int i = 0; i < users.size(); i++) {
                UserView userView = new UserView(users.get(i).getUsrId(), users.get(i).getAge(), users.get(i).getEmail(), users.get(i).getPhone(), users.get(i).getRole(), users.get(i).getUserName(), users.get(i).getGroups());
                usersView.add(userView);
            }

            return Response.ok().
                    entity(usersView).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response getDeletedUsers() {
        try {
            System.out.println("Getting deleted Users");
            List<User> users = repo.getDeletedUsers();
            List<UserView> usersView = new ArrayList<>();
            for (int i = 0; i < users.size(); i++) {
                UserView userView = new UserView(users.get(i).getUsrId(), users.get(i).getAge(), users.get(i).getEmail(), users.get(i).getPhone(), users.get(i).getRole(), users.get(i).getUserName(), users.get(i).getGroups());
                usersView.add(userView);
            }
            return Response.ok().
                    entity(usersView).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response getUser(@QueryParam("usrId") int usrId) {
        try {
            User user = repo.getUser(usrId);
            UserView userView = new UserView(user.getUsrId(), user.getAge(), user.getEmail(), user.getPhone(),
                    user.getRole(), user.getUserName(), user.getGroups());
            return Response.ok().
                    entity(userView).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response getAudit() {
        try {
            return Response.ok().
                    entity(repo.getAudit()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response undoDeletedUser(@QueryParam("usrId") int usrId) {
        if ((securityContext.isUserInRole("admin"))) {
            try {
                repo.undoDeletedUser(usrId, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("Can't undo delete while user");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }
        return Response.notModified("User can't undo delete a user").build();

    }


    @Override
    public Response addUser(User user) {

        System.out.println(securityContext.isUserInRole("admin"));
        System.out.println(securityContext.getUserPrincipal().toString());
        List<User> users = repo.getAllUsers();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getUserName().equals(user.getUserName())) {
                try {
                    throw new UserNameAlreadyExists("UserName Already Exists");
                } catch (UserNameAlreadyExists userNameAlreadyExists) {
                    userNameAlreadyExists.printStackTrace();
                }
            }
        }
        if (securityContext.isUserInRole("admin")) {
            try {
                return Response.ok().
                        entity(repo.addUser(user, securityContext.getUserPrincipal().toString())).
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }
        return Response.notModified("User can't create a new user").build();

    }

    @Override
    public Response updateUser(User user) {

        if ((securityContext.isUserInRole("admin")
                || securityContext.getUserPrincipal().getName().equals(user.getUserName()))
                && user.getUsrId() != 1) {
            try {
                repo.updateUser(user, securityContext.getUserPrincipal().toString());
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }


        return Response.notModified("User can't edit a user").build();
    }

    @Override
    public Response changePass(int usrId, String oldPass, String newPass) {
        User user = repo.getUser(usrId);
        if ((securityContext.getUserPrincipal().toString().equals(user.getUserName()))
                && usrId != 1) {
            try {
                repo.changePassword(usrId, oldPass, newPass, securityContext.getUserPrincipal().toString());
                return Response.ok().build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new OldPasswordsDoesNotMatch("Wrong old password");
        } catch (OldPasswordsDoesNotMatch oldPasswordsDoesNotMatch) {
            oldPasswordsDoesNotMatch.printStackTrace();
        }

        return Response.notModified("you are not an admin or the old password is incorrect").build();
    }


    @Override
    public Response deleteUser(@QueryParam("usrId") int usrId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                if (usrId != 1) {
                    repo.deleteUser(usrId, securityContext.getUserPrincipal().toString());
                    return Response.ok().
                            build();
                } else {
                    return Response.serverError().
                            entity("Can't delete the default admin").
                            build();
                }
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't delete a user ").build();

    }
}
