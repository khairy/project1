package users;

import Exceptions.OldPasswordsDoesNotMatch;
import audit.Audit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import groups.Group;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Stateless
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public UserRepository() {
    }

    public void audit(String action, String commiter, String timeOfAction, String json) {
        Audit audit = new Audit(action, commiter, timeOfAction, json);
        em.merge(audit);
    }

    public List<Audit> getAudit() {
        System.out.println("getting Audit noww");
        return em.createQuery("SELECT s FROM Audit s", Audit.class).
                getResultList();
    }

    public List<User> getAllUsers() {
        System.out.println("getting user noww");
        return em.createQuery("SELECT s FROM User s WHERE s.flag = 1", User.class).
                getResultList();
    }

    public List<User> getDeletedUsers() {
        return em.createQuery("SELECT s FROM User s WHERE s.flag = 0", User.class).
                getResultList();
    }

    public User getUser(int usrId) {
        System.out.println("getting user");
        User user = em.find(User.class, usrId);
        if (user != null) {
            if (user.isFlag()) {
                return user;
            } else {
                throw new NotFoundException("User is deleted");
            }
        }
        throw new NotFoundException("User is not in the database");
    }


    public User addUser(User entity, String currentUser) {

        try {
            if (entity.getRole().equals("admin")) {
                em.merge(entity);
                Group group = em.find(Group.class, 1);
                entity.getGroups().add(group);

            }
            entity.setPassword(sha256(entity.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            em.merge(entity);
            json = mapper.writeValueAsString(entity);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit("Add User", currentUser, json, date());
        em.merge(audit);
        return em.merge(entity);

    }

    public void updateUser(User entity, String currentUser) {
        if (entity.getPassword() == null) {
            User user = em.find(User.class, entity.getUsrId());

            entity.setPassword(user.getPassword());
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(entity));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Update User", currentUser, json, date());
            em.merge(audit);

            em.merge(entity);

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void deleteUser(int usrId, String currentUser) {
        User user = em.find(User.class, usrId);
        if (user == null)
            throw new IllegalArgumentException("User with ID: " + usrId + "does not exist in the database");
        try {
            user.setFlag(false);
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(user));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Delete User", currentUser, json, date());
            em.merge(audit);
            em.merge(user);

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void undoDeletedUser(int usrId, String currentUser) {
        User user = em.find(User.class, usrId);
        if (user == null)
            throw new IllegalArgumentException("User with ID: " + usrId + "does not exist in the database");
        try {
            user.setFlag(true);
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(user));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Undo Delete User", currentUser, json, date());
            em.merge(audit);
            em.merge(user);

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void changePassword(int usrId, String oldPass, String newPass, String currentUser) {
        User user = em.find(User.class, usrId);
        if (user == null)
            throw new IllegalArgumentException("User with ID: " + usrId + "does not exist in the database");
        try {
            if (sha256(oldPass).equals(user.getPassword())) {
                try {
                    user.setPassword(sha256(newPass));
                    ObjectMapper mapper = new ObjectMapper();
                    String json = null;
                    try {
                        json = mapper.writeValueAsString(em.merge(user));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    Audit audit = new Audit("Change Password", currentUser, json, date());
                    em.merge(audit);
                    em.merge(user);


                } catch (Exception e) {
                    throw e;
                }
            } else {
                throw new OldPasswordsDoesNotMatch("Old Passwords Does Not Match");

            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (OldPasswordsDoesNotMatch oldPasswordsDoesNotMatch) {
            oldPasswordsDoesNotMatch.printStackTrace();
        }
    }

    public static String sha256(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md5 = MessageDigest.getInstance("SHA-256");
        byte[] digest = md5.digest(input.getBytes("UTF-8"));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; ++i) {
            sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }

    public String date() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return "" + dateFormat.format(date);
    }
}

