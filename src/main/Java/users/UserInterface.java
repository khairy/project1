package users;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("user")
public interface UserInterface {

    @GET
    Response getAllUsers();

    @GET
    @Path("deleted")
    Response getDeletedUsers();

    @GET
    @Path("id")
    Response getUser(@QueryParam("usrId") int usrId);

    @GET
    @Path("audit")
    Response getAudit();

    @POST
    @Path("undo")
    Response undoDeletedUser(@QueryParam("usrId") int usrId);


    @POST
    Response addUser(User user);

    @POST
    @Path("update")
    Response updateUser(User user);

    @PUT
    @Path("changePass")
    Response changePass(@QueryParam("usrId") int usrId, @QueryParam("oldPass") String oldPass, @QueryParam("newPass") String newPass);

    @DELETE
    Response deleteUser(@QueryParam("usrId") int usrId);

}
