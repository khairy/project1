package users;

import groups.Group;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "users", schema = "project")
public class User implements Serializable {

    @Id
    @Column(name = "USRID")
    private int usrId;

    @Column(name = "AGE")
    private int age;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "ROLE")
    private String role;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "FLAG")
    private boolean flag;

    @ManyToMany(mappedBy = "users")
    @JoinTable(
            name = "GROUPMEMBERS",
            joinColumns = @JoinColumn(name = "USRID"),
            inverseJoinColumns = @JoinColumn(name = "GRPID"))
    private List<Group> groups;

    public User() {
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public User(int usrId, int age, String email, String phone, String role, String userName) {
        this.usrId = usrId;
        this.age = age;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.userName = userName;
    }

    public User(int usrId, int age, String email, String password, String phone, String role, String userName) {
        this.usrId = usrId;
        this.age = age;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.role = role;
        this.userName = userName;
    }

    public User(int age, String email, String password, String phone, String role, String userName) {
        this.age = age;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.role = role;
        this.userName = userName;
        this.flag = true;
    }

    @Override
    public String toString() {
        return "User{" +
                "usrId:" + usrId +
                ", Age:" + "'" + age + "'" + '\'' +
                ", email:" + email + '\'' +
                ", phone:" + phone + '\'' +
                ", role='" + role + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
