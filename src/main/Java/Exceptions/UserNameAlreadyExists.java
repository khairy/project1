package Exceptions;

public class UserNameAlreadyExists extends Exception {
    public UserNameAlreadyExists(String message) {
        super(message);
    }
}
