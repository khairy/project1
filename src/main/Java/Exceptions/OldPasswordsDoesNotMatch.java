package Exceptions;

public class OldPasswordsDoesNotMatch extends Exception {
    public OldPasswordsDoesNotMatch(String message) {
        super(message);
    }
}
