package tests;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import groups.Group;
import groups.GroupInterface;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GroupResourcesTests {
    private static GroupInterface client;

    @BeforeClass
    public static void init() {
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        client = JAXRSClientFactory.create("http://localhost:8880/project",
                GroupInterface.class, providers);
    }

    @Test
    public void test01AddGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response = client.addGroup(new Group(3, "test"));
        assertTrue("Cannot Add", response.getStatus() == 200);

        WebClient.client(client).reset();
        String user2 = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).reset();
        WebClient.client(client).header("Authorization", user2);
        Response response2 = client.addGroup(new Group(3, "test"));
        assertTrue("only admin can add", response2.getStatus() != 200);

    }

    @Test
    public void test02GetAllGroup() {
        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.getAllGroups();
        assertTrue("error in getting groups by admin", response.getStatus() == 200);

        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.getAllGroups();
        assertTrue("error in getting groups by user", response2.getStatus() == 200);

    }

    @Test
    public void test03DeleteGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.deleteGroup(3);
        assertTrue("error delete group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.deleteGroup(3);
        assertTrue("failed delete groups by admin", response.getStatus() == 200);


    }

    @Test
    public void test04GetGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.getGroup(3);
        assertTrue("error getting group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.getGroup(3);
        assertTrue("error getting group by group", response.getStatus() != 200);


    }

    @Test
    public void test05UndoRemoveGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.undoDeletedGroup(3);
        assertTrue("error undo deleting by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.undoDeletedGroup(3);
        assertTrue("error undo deleting group by admin", response.getStatus() == 200);

    }

    @Test
    public void test06UpdateGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.updateGroup(new Group(3, "TestGroups"));
        assertTrue("error updating group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.updateGroup(new Group(3, "TestGroups"));
        assertTrue("error updating group by admin", response.getStatus() == 200);

    }

    @Test
    public void test07AddToGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.addToGroup(1, 3);
        assertTrue("error add user to group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.addToGroup(1, 3);
        assertTrue("error add user to group by admin", response.getStatus() == 200);

    }

    @Test
    public void test08RemoveFromGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.removeFromGroup(1, 3);
        assertTrue("error removing user from group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.removeFromGroup(1, 3);
        assertTrue("error removing user from user by admin", response.getStatus() == 200);

    }

    @Test
    public void test09AddToGroupTest2() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.addToGroup(1, 3);
        assertTrue("error add user to group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.addToGroup(1, 3);
        assertTrue("error add user to group by admin", response.getStatus() == 200);

    }

    @Test
    public void test10MoveToGroup() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.moveToGroup(1, 3, 2);
        assertTrue("error moving user from to group by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.moveToGroup(1, 3, 2);
        assertTrue("error moving user from to group by admin", response.getStatus() == 200);

    }

}
