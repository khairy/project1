package tests;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import users.User;
import users.UserInterface;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;


@RunWith(JUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserResourcesTests {
    private static UserInterface client;

    @BeforeClass
    public static void init() {
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        client = JAXRSClientFactory.create("http://localhost:8880/project",
                UserInterface.class, providers);
    }

    @Test
    public void test01AddUser() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response = client.addUser(new User(3, 3, "KHA@gmail.com",
                "test", "0122248312", "admin", "test"));
        assertTrue("Cannot Add", response.getStatus() == 200);

        WebClient.client(client).reset();
        String user2 = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).reset();
        WebClient.client(client).header("Authorization", user2);
        Response response2 = client.addUser(new User(3, 4, "KHA@gmail.com",
                "user2", "0122248312", "user", "test2"));
        assertTrue("only admin can add", response2.getStatus() != 200);

    }

    @Test
    public void test02GetAllUsers() {
        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.getAllUsers();
        assertTrue("error in getting users by admin", response.getStatus() == 200);

        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.getAllUsers();
        assertTrue("error in getting users by user", response2.getStatus() == 200);

    }

    @Test
    public void test03DeleteUser() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.deleteUser(3);
        assertTrue("error", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.deleteUser(3);
        assertTrue("failed delete user by admin", response.getStatus() == 200);


    }

    @Test
    public void test04GetUser() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.getUser(3);
        assertTrue("error getting user by admin", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.getUser(3);
        assertTrue("error getting user by user", response.getStatus() != 200);


    }

    @Test
    public void test05UndoRemoveUser() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.undoDeletedUser(3);
        assertTrue("error undo deleting by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.undoDeletedUser(3);
        assertTrue("error undo deleting user by admin", response.getStatus() == 200);

    }

    @Test
    public void test06UpdateUser() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.updateUser(new User(3, 3, "KHA@gmail.com",
                "0122248312", "admin", "test2"));
        assertTrue("error updating user by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.updateUser(new User(3, 3, "KHA@gmail.com",
                "0122248312", "admin", "test2"));
        assertTrue("error updating user by admin", response.getStatus() == 200);

    }

    @Test
    public void test07ChangePassword() {
        WebClient.client(client).reset();
        String user = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", user);
        Response response2 = client.changePass(3, "test", "test2");
        assertTrue("error updating pass by user", response2.getStatus() != 200);

        WebClient.client(client).reset();
        String admin = "Basic " + Base64Utility.encode(("admin:admin").getBytes());
        WebClient.client(client).header("Authorization", admin);
        Response response = client.changePass(3, "test", "test2");
        assertTrue("error updating pass by admin", response.getStatus() != 200);

        WebClient.client(client).reset();
        String test = "Basic " + Base64Utility.encode(("user:user").getBytes());
        WebClient.client(client).header("Authorization", test);
        Response response3 = client.changePass(2, "user", "user2");
        assertTrue("error updating pass by test", response3.getStatus() == 200);

    }


}
