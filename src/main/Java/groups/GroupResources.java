package groups;

import Exceptions.UnAuthorized;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class GroupResources implements GroupInterface {

    private static final Logger LOGGER = Logger.getLogger(GroupResources.class.getName());

    @EJB
    private GroupRepository repo;

    @Context
    HttpServletRequest request;

    @Context
    SecurityContext securityContext;


    @Override
    public Response getAllGroups() {
        try {
            return Response.ok().
                    entity(repo.getAllGroups()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response getDeletedGroups() {
        try {
            return Response.ok().
                    entity(repo.getDeletedGroups()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @Override
    public Response getGroup(@QueryParam("grpId") int grpId) {
        Group group = repo.getGroup(grpId);
        if (group.isFlag()) {
            try {
                return Response.ok().
                        entity(repo.getGroup(grpId)).
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        return Response.notModified("The group is deleted").build();

    }

    @Override
    public Response addGroup(Group group) {
        if (securityContext.isUserInRole("admin")) {
            try {
                repo.addGroup(group, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't create a new group").build();
    }

    @Override
    public Response updateGroup(Group group) {
        if (securityContext.isUserInRole("admin")) {
            try {
                repo.updateGroup(group, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't update a group").build();
    }

    @Override
    public Response deleteGroup(@QueryParam("grpId") int grpId) {
        if (securityContext.isUserInRole("admin")) {
            try {

                repo.deleteGroup(grpId, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            Response.notModified("User can't delete a group").build();
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't delete a group").build();

    }

    @Override
    public Response undoDeletedGroup(@QueryParam("grpId") int grpId) {
        if (securityContext.isUserInRole("admin")) {

            try {
                repo.undoDeletedGroup(grpId, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }
        return Response.notModified("User can't Undo deleted group").build();

    }

    @Override
    public Response addToGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId) {
        if (securityContext.isUserInRole("admin")) {
            try {
                repo.addToGroup(usrId, grpId, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }
        return Response.notModified("User can't add to group").build();
    }

    @Override
    public Response removeFromGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId) {
        if (securityContext.isUserInRole("admin")) {

            try {
                repo.removeFromGroup(usrId, grpId, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't remove from group").build();

    }

    @Override
    public Response moveToGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId, @QueryParam("grpId") int grpId2) {
        if (securityContext.isUserInRole("admin")) {
            try {
                repo.moveToGroup(usrId, grpId, grpId2, securityContext.getUserPrincipal().toString());
                return Response.ok().
                        build();
            } catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage()).
                        build();
            }
        }
        try {
            throw new UnAuthorized("You are unAuthorized");
        } catch (UnAuthorized unAuthorized) {
            unAuthorized.printStackTrace();
        }

        return Response.notModified("User can't move from or to group").build();
    }

}
