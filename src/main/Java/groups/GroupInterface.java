package groups;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("group")
public interface GroupInterface {

    @GET
    Response getAllGroups();

    @GET
    @Path("deleted")
    Response getDeletedGroups();

    @GET
    @Path("id")
    Response getGroup(@QueryParam("grpId") int grpId);

    @POST
    Response addGroup(Group group);

    @POST
    @Path("update")
    Response updateGroup(Group group);

    @DELETE
    Response deleteGroup(@QueryParam("grpId") int grpId);

    @POST
    @Path("undo")
    Response undoDeletedGroup(@QueryParam("grpId") int grpId);

    @POST
    @Path("addToGroup")
    Response addToGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId);

    @DELETE
    @Path("removeFromGroup")
    Response removeFromGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId);

    @POST
    @Path("moveToGroup")
    Response moveToGroup(@QueryParam("usrId") int usrId, @QueryParam("grpId") int grpId, @QueryParam("grpId2") int grpId2);
}
