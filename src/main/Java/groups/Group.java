package groups;

import com.fasterxml.jackson.annotation.JsonIgnore;
import users.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "groupstable", schema = "project")
public class Group {

    public Group() {
    }

    @Id
    @Column(name = "GRPID", updatable = false, nullable = false)
    private int grpId;

    @Column(name = "GROUPNAME")
    private String groupName;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "GROUPMEMBERS",
            joinColumns = @JoinColumn(name = "GRPID"),
            inverseJoinColumns = @JoinColumn(name = "USRID"))
    private List<User> users;

    @Column(name = "FLAG")
    private boolean flag;

    public Group(int grpId, String groupName) {
        this.grpId = grpId;
        this.groupName = groupName;
        this.flag = true;
    }

    public int getGrpId() {
        return grpId;
    }

    public void setGrpId(int grpId) {
        this.grpId = grpId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Group{" +
                "grpId=" + grpId +
                ", groupName='" + groupName + '\'' +
                ", users=" + users +
                ", flag=" + flag +
                '}';
    }
}
