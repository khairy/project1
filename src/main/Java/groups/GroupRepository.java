package groups;

import audit.Audit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import users.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Stateless
public class GroupRepository {
    @PersistenceContext
    private EntityManager em;

    public GroupRepository() {
    }

    public List<User> getAllGroups() {
        return em.createQuery("SELECT s FROM Group s WHERE s.flag = 1", User.class).
                getResultList();
    }

    public List<User> getDeletedGroups() {
        return em.createQuery("SELECT s FROM Group s WHERE s.flag = 0", User.class).
                getResultList();
    }

    public Group getGroup(int grpId) {
        return em.find(Group.class, grpId);
    }

    public Group addGroup(Group entity, String currentUser) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(em.merge(entity));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Audit audit = new Audit("Add group", currentUser, json, date());
        em.merge(audit);
        return em.merge(entity);

    }

    public void updateGroup(Group entity, String currentUser) {
        if (entity.getGrpId() == 1) {
            try {
                throw new Exception("Can't Edit default admins group");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(entity));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Update group", currentUser, json, date());
            em.merge(audit);
            em.merge(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteGroup(int grpId, String currentUser) {
        if (grpId == 1) {
            try {
                throw new Exception("Can't Delete default admins group");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Group group = em.find(Group.class, grpId);
        if (group == null)
            throw new IllegalArgumentException("User with ID: " + group + "does not exist in the database");
        try {
            group.setFlag(false);
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(group));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Delete Group", currentUser, json, date());
            em.merge(audit);
            em.merge(group);
        } catch (Exception e) {
            throw e;
        }
    }

    public void undoDeletedGroup(int grpId, String currentUser) {
        Group group = em.find(Group.class, grpId);
        if (group == null)
            throw new IllegalArgumentException("Group with ID: " + grpId + "does not exist in the database");
        try {
            group.setFlag(true);
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(group));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Undo Deleted group", currentUser, json, date());
            em.merge(audit);
            em.merge(group);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addToGroup(int usrId, int grpId, String currentUser) {
        User user = em.find(User.class, usrId);
        Group group = em.find(Group.class, grpId);

        if (user == null)
            throw new IllegalArgumentException("User with ID: " + usrId + "does not exist in the database");
        if (group == null)
            throw new IllegalArgumentException("group with ID: " + grpId + "does not exist in the database");

        if (grpId == 1 && user.getRole().equals("user")) {
            try {
                throw new Exception("user with ID: " + usrId + "can't join admins group");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            user.getGroups().add(group);
            group.getUsers().add(user);
            System.out.println("IN");
            em.merge(user);
            em.merge(group);
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            try {
                json = mapper.writeValueAsString(em.merge(group));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Audit audit = new Audit("Add to Groupr", currentUser, json, date());
            em.merge(audit);
            System.out.println("after");
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeFromGroup(int usrId, int grpId, String currentUser) {
        User user = em.find(User.class, usrId);
        if (user == null)
            throw new IllegalArgumentException("User with ID: " + usrId + "does not exist in the database");

        Group group = em.find(Group.class, grpId);
        if (group == null)
            throw new IllegalArgumentException("Group with ID: " + grpId + "does not exist in the database");

        if (grpId == 1 && user.getRole().equals("admin")) {
            try {
                System.out.println("Cannot remove admin from admins group");
                throw new Exception("admin with ID: " + usrId + "can't be removed from admins group");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                for (int i = 0; i < user.getGroups().size(); i++) {
                    if (user.getGroups().get(i).getGrpId() == group.getGrpId()) {
                        user.getGroups().remove(i);

                    }
                }
                for (int i = 0; i < group.getUsers().size(); i++) {
                    if (group.getUsers().get(i).getUsrId() == user.getUsrId()) {
                        group.getUsers().remove(i);

                    }
                }
                ObjectMapper mapper = new ObjectMapper();
                String json = null;
                try {
                    json = mapper.writeValueAsString(em.merge(group));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                Audit audit = new Audit("Remove User from group", currentUser, json, date());
                em.merge(audit);
                em.merge(user);
                em.merge(group);
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void moveToGroup(int usrId, int grpId, int grpId2, String currentUser) {
        removeFromGroup(usrId, grpId, currentUser);
        addToGroup(usrId, grpId2, currentUser);

    }

    public String date() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return "" + dateFormat.format(date);
    }

}


